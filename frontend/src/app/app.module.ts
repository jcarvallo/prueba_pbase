import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { APP_BASE_HREF, Location } from '@angular/common';

// componentes
import { NotFoundComponent } from './global/not-found/index';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/index';

// plugings
import { ToastyModule } from 'ng2-toasty';
import { ModalModule } from 'ngx-bootstrap';

// environment
import { environment } from '../environments/environment';

// guard
import { AuthGuard } from './guards/index';

// service
import { AuthService } from './services/index';

@NgModule({
  declarations: [
    NotFoundComponent,
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ToastyModule.forRoot(),
    ModalModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    AuthGuard,
    AuthService,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    {
      provide: APP_BASE_HREF,
      useValue: environment.baseUrl
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
