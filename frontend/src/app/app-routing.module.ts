import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// COMPONENETS
import { NotFoundComponent } from './global/not-found/index';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/index';

// GUARDS PARA ADMIN Y ALUMNOS
import { AuthGuard } from './guards/index';

const routes: Routes = [
  {
    path: '',
    loadChildren: './layout/layout.module#LayoutModule',
    canActivate: [AuthGuard]
  },
  { path: 'login', component: LoginComponent },

  // otherwise redirect to home
  { path: '**', component: NotFoundComponent },
  { path: 'not-found', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
