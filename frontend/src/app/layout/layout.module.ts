import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule, NgModel } from '@angular/forms';

// plugines
import { ToastyModule } from 'ng2-toasty';
import { ModalModule } from 'ngx-bootstrap';
import { QuillModule } from 'ngx-quill'
import { MyDatePickerModule } from 'mydatepicker';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { TextMaskModule } from 'angular2-text-mask';
import { FileUploadModule } from 'primeng/primeng';

// componentes
import { GrillaComponent } from './grillas/index';
import { LayoutRoutingModule, LayoutComponent } from './index';
import { HeaderComponent, SidebarComponent, FooterComponent } from './index';
import { HomeComponent } from './home/index';
import { LoaderComponent } from './loader/index';

import { ConfirmarCambioComponent } from '../global/confirmar-cambios/index';

import { UserComponent } from './users/index';
import { CreateUserComponent } from './users/create/index';
import { EditUserComponent } from './users/edit/index';

@NgModule({
   imports: [
      CommonModule,
      LayoutRoutingModule,
      FormsModule,
      ReactiveFormsModule,
      ToastyModule.forRoot(),
      ModalModule.forRoot(),
      QuillModule,
      MyDatePickerModule,
      MyDateRangePickerModule,
      TextMaskModule,
      FileUploadModule
   ],
   declarations: [
      GrillaComponent,
      LayoutComponent,
      HeaderComponent,
      SidebarComponent,
      FooterComponent,
      HomeComponent,
      LoaderComponent,
      ConfirmarCambioComponent,
      UserComponent,
      CreateUserComponent,
      EditUserComponent
   ]
})
export class LayoutModule { }
