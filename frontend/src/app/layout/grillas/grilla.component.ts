import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, NgForm, FormGroup } from '@angular/forms';
import { UserService, RolService } from '../../services/index'
import { AlertServer, TextMask } from '../../global/index';
import { User, Rol } from '../../models/index';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
   moduleId: module.id,
   selector: 'grilla',
   templateUrl: './grilla.html',
   providers: [UserService, RolService, AlertServer]
})

export class GrillaComponent implements OnInit {
   @Input()  columns: any[];
   @Input()  acciones: any[];
   @Input()  listObjects: any[];
   @Output() send= new EventEmitter();

   constructor(
      private router: Router,
      private notificacion: AlertServer,
   ) {

   }

   // CARGAR AUTOMATICA
   ngOnInit() {
      // console.log(this.listObjects);
   }

   // ENVIAR DATOS AL PADRE
   sendToPather(accion: any, object: any, index: number){
      this.send.emit({ accion: accion, data: object, index: index });
   }

}
