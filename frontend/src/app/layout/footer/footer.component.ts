import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';

@Component({
    moduleId: module.id,
    selector: 'footer',
    templateUrl: './footer.html',
})

export class FooterComponent implements OnInit{
  version = environment.version;

  ngOnInit(){
    this.version;
  }

  // SCROLL HACIA ARRIBA
  scrollTop() {
    $('body, html').animate({
      scrollTop: '0px'
    }, 300);
  }
}
