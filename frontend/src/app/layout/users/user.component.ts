import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, NgForm, FormGroup } from '@angular/forms';
import { UserService, RolService } from '../../services/index'
import { AlertServer, TextMask } from '../../global/index';
import { User, Rol } from '../../models/index';
import { ModalDirective } from 'ngx-bootstrap';
import { CreateUserComponent } from './create/index';
import { EditUserComponent } from './edit/index';

@Component({
   moduleId: module.id,
   selector: 'user',
   templateUrl: './user.html',
   providers: [UserService, RolService, AlertServer]
})

export class UserComponent implements OnInit {
   @ViewChild('modalCrear') modalCrear: CreateUserComponent;
   @ViewChild('modalEditar') modalEditar: EditUserComponent;
   @ViewChild('modalEliminar') modalEliminar: ModalDirective;
   listUsers: any[] = [];
   cmbRoles: Rol[] = [];
   pagina: number = -1;
   isEmptyList: boolean = false;
   dataDelete: User;
   indexDelete: number;
   loader: boolean = false;
   loaderPrincipal: boolean= false;
   proximaPagina: string;

   // PATTERN PARA SOLO NUMEROS
   onlyNumberMask = TextMask.onlyNumberMask;

   // FILTRADO
   filterFields: any = {
      nombre: '',
      apellido: '',
      email: '',
      telefono: '',
      habilitado: 1,
      rol: '',
      nombre_ord: '',
      apellido_ord: '',
      email_ord: '',
      telefono_ord: '',
      rol_ord: ''
   };

   // COLUMNAS
   columns: any[]= [
      {
         header:  'Nombre'
      },
      {
         header: 'Apellido'
      },
      {
         header: 'Email'
      },
      {
         header: 'Teléfono'
      },
      {
         header: 'Rol'
      },
      {
         header: '¿Habilitado?'
      },
      {
         header: 'Acciones'
      }
   ]

   // ACCIONES
   acciones: any[] = [
      {
         label: 'Editar',
         icon: 'fa-pencil',
         type: 'editar'
      },
      {
         label: 'Eliminar',
         icon: 'fa-trash',
         type: 'eliminar'
      }
   ]


   constructor(
      private servicio: UserService,
      private servicioRol: RolService,
      private router: Router,
      private notificacion: AlertServer,
   ) {

   }

   // CARGAR AUTOMATICA
   ngOnInit() {
      this.loadFilter();
      this.loadCmbRoles();
   }

   // GET DE ACCIONES DE LA GRILLA
   getAccion(event: any){

      switch(event.accion.type){
         case 'editar':{
            this.modalEditar.showModal(event.data,event.index);
            break;
         }
         case 'eliminar':{
            this.showModalEliminar(event.data,event.index);
            break;
         }

      }
      console.log(event);
   }

   // FORMATEAMOS EL ARRAY DE

   // cargar combo de roles
   loadCmbRoles(): void {
      this.servicioRol.getCmbRoles().subscribe(
         data => {
            this.cmbRoles= data.data;
         },
         error => {
            this.notificacion.messageError(error);
         }
      );
   }

   // CAPTURAR EL ORDENAMIENTO
   orderFilter(columna: string, orden: string) {
      this.filterFields.nombre_ord = '';
      this.filterFields.apellido_ord = '';
      this.filterFields.email_ord = '';
      this.filterFields.telefono_ord = '';
      this.filterFields.rol_ord = '';

      if (columna == 'nombre_ord') this.filterFields.nombre_ord = orden;
      if (columna == 'apellido_ord') this.filterFields.apellido_ord = orden;
      if (columna == 'email_ord') this.filterFields.email_ord = orden;
      if (columna == 'telefono_ord') this.filterFields.telefono_ord = orden;
      if (columna == 'rol_ord') this.filterFields.rol_ord = orden;

      this.filter(this.filterFields);
   }

   // CARGAR BUSQUEDA EN FILTRO
   loadSearch() {
      this.search(this.filterFields);
   }

   // CARGAR FILTRO
   loadFilter() {
      this.filter(this.filterFields);
   }

   // APLICAR FILTRADO
   filter(objectFilter: any) {
      this.pagina = -1;
      this.listUsers = [];
      this.search(objectFilter);
   }

   // FUNCIONAMIENTO DE BOTON VER MÁS
   search(objectFilter: any) {
      this.pagina++;
      this.filterFields.pagina = this.pagina;
      this.filterFields = objectFilter;
      this.loaderPrincipal= true;

      // PETICION PARA VERIFICAR SI LA PROXIMA PAGINA TIENE DATOS
      this.filterFields.pagina++;
      this.servicio.getFilter(this.filterFields).subscribe(
         (data) => {
            this.verificarProximaPagina(data.data);

            // PETICION PARA MOSTRAR DATOS
            this.filterFields.pagina= this.pagina;
            this.servicio.getFilter(this.filterFields).subscribe(
               (data) => {
                  this.cargarLista(data.data);
                  this.loaderPrincipal= false;
               },
               error => {
                  this.notificacion.messageError(error);
                  this.loaderPrincipal= false;
               }
            );
         },
         error => {
            this.notificacion.messageError(error);
            this.loaderPrincipal= false;
         }
      );
   }

   // CARGAR Y CONCATENAR DATA
   cargarLista(data: User[]) {
      this.listUsers = this.listUsers.concat(data);
      this.isEmptyList = false
      if (this.listUsers.length < 1) this.isEmptyList = true;

      this.loadContent(this.listUsers);
   }

   // CARGAR CONTENIDO DE COLUMNAS
   loadContent(users: User[]){
      this.listUsers= [];
      // itero por
      for (let user of users) {
         // Iteramos cmbRoles
         for (let rol of this.cmbRoles) {
            if(rol.id==user.rol_id) var rol_name=rol.nombre;
         }

         var model=this.formatArrayData(user,rol_name);
         this.listUsers.push(model);
      }
   }

   // VERIFICAR DATOS DE PROXIMA PAGINA
   verificarProximaPagina(data: User[]){
      this.proximaPagina= "vacia";
      if (data.length > 0) this.proximaPagina= "llena";
      return this.proximaPagina;
   }

   // RESET FILTRADO
   resetFilter() {
      this.filterFields.nombre = '';
      this.filterFields.apellido = '';
      this.filterFields.email = '';
      this.filterFields.telefono = '';
      this.filterFields.rol = '';
      this.filterFields.habilitado = 1,
      this.filterFields.nombre_ord = '';
      this.filterFields.apellido_ord = '';
      this.filterFields.email_ord = '';
      this.filterFields.telefono_ord = '';
      this.filterFields.rol_ord = '';

      this.loadFilter();
   }

   // ACTUALIZAR REGISTRO EN LA LISTA
   actualizarListaEdicion(event: any) {

   // Iteramos cmbRoles
   for (let rol of this.cmbRoles) {
      if(rol.id==event.data.rol_id) var rol_name=rol.nombre;
   }

   var model=this.formatArrayData(event.data,rol_name);

      this.listUsers[event.index] = model;
   }

   // MOSTRAR MODAL ELIMINACION
   showModalEliminar(data: any, index: number): void {
      this.modalEliminar.show();
      for (let i of data) {
         switch(i.header){
            case 'Nombre':{
               var nombre=i.content;
               var id=i.id;
               break;
            }
            case 'Apellido':{
               var apellido=i.content;
               break;
            }
         }

      }
      var model:any={id:id,nombre:nombre,apellido:apellido};
      this.dataDelete = model;
      this.indexDelete = index;
   }

   // OCULTAR MODAL ELIMINACION
   hideModalEliminar(): void {
      this.modalEliminar.hide();
   }

   // ELIMINAR LA MATERIA
   delete(): void {
      this.loader = true;
      console.log(this.dataDelete.id)
      this.servicio.delete(this.dataDelete.id)
      .subscribe(
         data => {
            this.actualizarListaEliminacion();
            this.notificacion.message("Se eliminó el usuario", "warning");
            this.loader = false;
            this.hideModalEliminar();
         },
         error => {
            this.notificacion.messageError(error);
            this.loader = false;
         }
      );
   }

   // ACTUALIZAR LISTA ELIMINACION
   actualizarListaEliminacion() {
      this.listUsers.splice(this.indexDelete, 1);
   }

   formatArrayData(data:any,rol_name:string){

      var model: any[]= [
         {
            id: data.id,
            content: data.nombre,
            header: "Nombre"
         },
         {
            id: data.id,
            content: data.apellido,
            header: "Apellido"
         },
         {
            id: data.id,
            content: data.email,
            header: "Email"
         },
         {
            id: data.id,
            content: data.telefono,
            header: "Teléfono"
         },
         {
            id: data.id,
            content: rol_name,
            header: "Rol",
            rol_id:data.rol_id
         },
         {
            id: data.id,
            content: data.habilitado ? 'Sí' : 'No',
            header: "¿Habilitado?",
            habilitado_id: data.habilitado
         }
      ]

      return model;

   }



}
