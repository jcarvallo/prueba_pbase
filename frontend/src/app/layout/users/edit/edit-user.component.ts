import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, NgForm, FormGroup } from '@angular/forms';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';
import { UserService } from '../../../services/index'
import { AlertServer, TextMask } from '../../../global/index';
import { User, Rol } from '../../../models/index';
import { ModalDirective } from 'ngx-bootstrap';
import { ConfirmarCambioComponent } from '../../../global/confirmar-cambios/index';
import { Validation } from '../../../global/index';

@Component({
  moduleId: module.id,
  selector: 'edit-user',
  templateUrl: './edit-user.html',
  providers: [UserService, AlertServer]
})

export class EditUserComponent implements OnInit {
  @ViewChild('modalEditar') modalEditar: ModalDirective;
  @ViewChild('modalConfirmarCambio') modalConfirmarCambio: ConfirmarCambioComponent;
  @Input() cmbRoles: Rol[];
  @Output() actualizarFila = new EventEmitter();
  formEdicion: FormGroup;
  submitForm: boolean= false;
  loader: boolean= false;
  dataUser: User;
  positionList: number;
  formChange: boolean= false;

  // PATTERN PARA SOLO NUMEROS
  onlyNumberMask= TextMask.onlyNumberMask;

  constructor(
    private router: Router,
    private servicio: UserService,
    private fb: FormBuilder,
    private notificacion: AlertServer,
  ) {
  }

  // CARGA AUTOMATICA
  ngOnInit() {
  }

  // ABRIR MODAL DE CREACION
  showModal(data: any, index: number): void {
    this.modalEditar.show();
    this.positionList= index;

    for (let i of data) {
      switch(i.header){
         case 'Nombre':{
            var nombre=i.content;
            var id=i.id;
            break;
         }
         case 'Apellido':{
            var apellido=i.content;
            break;
         }
         case 'Email':{
            var email=i.content;
            break;
         }
         case 'Teléfono':{
            var telefono=i.content;
            break;
         }
         case 'Rol':{
            var rol_id=i.rol_id;
            break;
         }
         case '¿Habilitado?':{
            var habilitado_id=i.habilitado_id;
            break;
         }

      }
      var user:any={
         id:id,
         nombre:nombre,
         apellido:apellido,
         email:email,
         telefono:telefono,
         rol_id:rol_id,
         habilitado_id:habilitado_id
      }

    }
    this.dataUser= user;

    console.log(data)

    this.formEdicion= this.fb.group({
      nombre: [user.nombre, Validators.compose([
          Validators.required,
          Validation.verificarEspacios,
      ])],
      apellido: [user.apellido, Validators.compose([
          Validators.required,
          Validation.verificarEspacios,
      ])],
      email: [user.email, Validators.compose([
          Validators.required,
          Validation.verificarEspacios,
          Validators.pattern(/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/im)
      ])],
      telefono: [user.telefono, Validators.compose([
          Validators.required,
          Validation.verificarEspacios,
      ])],
      rol_id: [user.rol_id, Validators.compose([
          Validators.required,
      ])],
      habilitado: [user.habilitado_id, Validators.compose([
          Validators.required,
      ])],
    })
  }

  // VERIFICAR SI HUBO ALGUN CAMBIO EN EL FORMULARIO
  verificarCambio(): void{
    this.formChange= true;
  }

  // CERRAR MODAL DE CREACION
  hideModal(): void {
    if (this.formChange== true) {
      this.modalConfirmarCambio.showModal();
    }else{
      this.modalEditar.hide();
      this.formEdicion.reset();
      this.formChange= false;
      this.submitForm= false;
    }
  }

  // CERRAR MODAL DE EDICION SI NO DECIDE GUARDAR LOS CAMBIO
  cerrarModal(): void{
    this.modalEditar.hide();
    this.formEdicion.reset();
    this.formChange= false;
    this.submitForm= false;
  }

  // EDITAR LA MATERIA
  edit(values: any): void{
    this.submitForm= true;
    if (this.formEdicion.valid && this.submitForm) {
      this.loader= true;
      this.servicio.update(values, this.dataUser.id)
      .subscribe(
          data => {
              this.cerrarModal();
              this.actualizarFila.emit({ data: data.data, index: this.positionList });
              this.notificacion.message("Se modificó el usuario", "info");
              this.loader= false;
          },
          error => {
              this.notificacion.messageError(error);
              this.loader= false;
              this.submitForm= false;
          }
      );
    }else{
      this.formEdicion= this.fb.group({
        nombre: [values.nombre, Validators.compose([
            Validators.required,
            Validation.verificarEspacios,
        ])],
        apellido: [values.apellido, Validators.compose([
            Validators.required,
            Validation.verificarEspacios,
        ])],
        email: [values.email, Validators.compose([
            Validators.required,
            Validation.verificarEspacios,
            Validators.pattern(/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/im)
        ])],
        telefono: [values.telefono, Validators.compose([
            Validators.required,
            Validation.verificarEspacios,
        ])],
        rol_id: [values.rol_id, Validators.compose([
            Validators.required,
        ])],
        habilitado: [values.habilitado, Validators.compose([
            Validators.required,
        ])],
      })
    }
  }

}
