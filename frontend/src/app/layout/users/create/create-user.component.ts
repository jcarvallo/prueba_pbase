import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, NgForm, FormGroup } from '@angular/forms';
import { UserService } from '../../../services/index'
import { AlertServer, ConfigEditor } from '../../../global/index';
import { User, Rol } from '../../../models/index';
import { ModalDirective } from 'ngx-bootstrap';
import { Validation, TextMask } from '../../../global/index';

@Component({
  moduleId: module.id,
  selector: 'create-user',
  templateUrl: './create-user.html',
  providers: [UserService, AlertServer]
})

export class CreateUserComponent implements OnInit {
  @ViewChild('modalCrear') modalCrear: ModalDirective;
  @Input() cmbRoles: Rol[];
  @Output() refrescarListado = new EventEmitter();
  formCreacion: FormGroup;
  submitForm: boolean= false;
  loader: boolean= false;

  // PATTERN PARA SOLO NUMEROS
  onlyNumberMask= TextMask.onlyNumberMask;

  constructor(
    private router: Router,
    private servicio: UserService,
    private fb: FormBuilder,
    private notificacion: AlertServer,
  ){
  }

  // CARGA AUTOMATICA
  ngOnInit() {
    this.createForm();
  }

  // ABRIR MODAL DE CREACION
  showModal(): void {
    this.modalCrear.show();
  }

  // CERRAR MODAL DE CREACION
  hideModal(): void {
    this.modalCrear.hide();
    this.submitForm= false;
    this.formCreacion.reset({ rol_id: '', habilitado: 1});
  }

  // CREACION DE FORMULARIO
  createForm(): void{
    this.formCreacion= this.fb.group({
      nombre: ['', Validators.compose([
          Validators.required,
          Validation.verificarEspacios,
      ])],
      apellido: ['', Validators.compose([
          Validators.required,
          Validation.verificarEspacios,
      ])],
      email: ['', Validators.compose([
          Validators.required,
          Validation.verificarEspacios,
          Validators.pattern(/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/im)
      ])],
      telefono: ['', Validators.compose([
          Validators.required,
          Validation.verificarEspacios,
      ])],
      rol_id: ['', Validators.compose([
          Validators.required,
      ])],
      habilitado: [1, Validators.compose([
          Validators.required,
      ])],
    })
  }

  //GUARDAR MATERIA
  save(values: any): void {
    this.submitForm= true;
    if (this.formCreacion.valid && this.submitForm) {
      this.loader= true;
      this.servicio.create(values)
      .subscribe(
          data => {
              this.notificacion.message("Se creó el usuario", "success");
              this.hideModal();
              this.refrescarListado.emit();
              this.loader= false;
          },
          error => {
              this.notificacion.messageError(error);
              this.loader= false;
              this.submitForm= false;
          }
      );
    }else{
      this.formCreacion= this.fb.group({
        nombre: [values.nombre, Validators.compose([
            Validators.required,
            Validation.verificarEspacios,
        ])],
        apellido: [values.apellido, Validators.compose([
            Validators.required,
            Validation.verificarEspacios,
        ])],
        email: [values.email, Validators.compose([
            Validators.required,
            Validation.verificarEspacios,
            Validators.pattern(/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/im)
        ])],
        telefono: [values.telefono, Validators.compose([
            Validators.required,
            Validation.verificarEspacios,
        ])],
        rol_id: [values.rol_id, Validators.compose([
            Validators.required,
        ])],
        habilitado: [values.habilitado, Validators.compose([
            Validators.required,
        ])],
      })
    }
  }

}
