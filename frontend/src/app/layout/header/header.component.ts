import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/index';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';
import { environment } from '../../../environments/environment';
import { AlertServer } from '../../global/index';

@Component({
  moduleId: module.id,
  selector: 'header',
  templateUrl: './header.html',
  providers: [AlertServer]
})

export class HeaderComponent implements OnInit {
  currentUser: any;
  apiUrl = environment.apiUrl;

  constructor(
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig,
    private authenticationService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
  ) {

    // OPCIONES PREDETERMINADAS TASTY
    this.toastyConfig.theme = 'bootstrap';
    this.toastyConfig.timeout = 5000;
    this.toastyConfig.showClose = true;
  }

  // INICIO AUTOMATICO
  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  // CERRAR SESION E INVALIDAR TOKEN
  logout() {
    $.ajax({
      url: this.apiUrl + 'logout',
      headers: { 'Authorization': 'Bearer ' + this.currentUser.token },
      type: 'GET',
      success: this.router.navigate(['/login'])
    });
  }

}
