import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// componenetes
import { LayoutComponent } from './index';
import { HomeComponent } from './home/index';
import { UserComponent } from './users/index';

const routes: Routes = [
  {
    path: '', component: LayoutComponent,
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'users', component: UserComponent }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
