import { Component, OnInit } from '@angular/core';
import { ConfigEditor } from '../../global/index';

@Component({
   moduleId: module.id,
   templateUrl: './home.html',
})

export class HomeComponent implements OnInit{
   currentDate: Date;

   constructor(){}

   ngOnInit(){
      setInterval(() => this.currentDate = new Date(), 1000);
   }

}
