export class FormateDateRange{

  // FORMATEAR RABNGO DE FECHAS (INICIO - FINAL)
  public static formatearRango(fecha: any){
    if (fecha) {
      var dateInicio= new Date(fecha.beginJsDate);
      var fechaInicioObject= {date: { year: dateInicio.getUTCFullYear(), month: dateInicio.getUTCMonth()+1, day: dateInicio.getDate() }};
      var fechaInicio= fechaInicioObject.date.year +"-"+ fechaInicioObject.date.month +"-"+ fechaInicioObject.date.day;

      var dateFinal= new Date(fecha.endJsDate);
      var fechaFinalObject= {date: { year: dateFinal.getUTCFullYear(), month: dateFinal.getUTCMonth()+1, day: dateFinal.getDate() }};
      var fechaFinal= fechaFinalObject.date.year +"-"+ fechaFinalObject.date.month +"-"+ fechaFinalObject.date.day;
    }else{
      fechaInicio= null;
      fechaFinal= null;
    }

    return { inicio: fechaInicio, final: fechaFinal }
  }
}
