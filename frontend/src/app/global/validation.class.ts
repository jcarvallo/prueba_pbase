import { AbstractControl } from '@angular/forms';

export class Validation{

	// VERIFICAR ESPACIOS EN BLANCO
	static verificarEspacios(c: AbstractControl){

		var espacios= /^\s+/g;
		if (c.value) {
		   var campo= c.value.replace(/<\/?[^>]+(>|$)/g, "");
		}else{
			 var campo= null;
		}

		if(campo == null || campo == [] || campo == "") {
			return null;
		}

		if(campo.search(espacios) >= 0) {
			return { sinEspacios: true }
		}

		return null;

	}

}
