export class ConfigEditor{

  public static toolbar: any =
  {
      toolbar: [
        ['bold', 'italic', 'strike', 'underline'],
        [{ 'align': [] }, { 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'color': [] }, { 'background': [] }],
        ['image'],
        ['link']
      ]
  };

}
