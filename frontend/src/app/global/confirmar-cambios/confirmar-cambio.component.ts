import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AlertServer } from '../../global/index';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  moduleId: module.id,
  selector: 'confirmar-cambio',
  templateUrl: './confirmar-cambio.html'
})

export class ConfirmarCambioComponent implements OnInit {
  @ViewChild('modalConfirmarCambio') modalConfirmarCambio: ModalDirective;
  @Output() salirYGuardarCambios= new EventEmitter();
  @Output() salirSinGuardarCambios= new EventEmitter();

  constructor(){

  }

  // CARGA AUTOMATICA
  ngOnInit(){

  }

  // ABRIR MODAL DE CREACION
  showModal(): void {
    this.modalConfirmarCambio.show();
  }

  // SALIR Y GUARDAR CAMBIOS
  salirYGuardar(): void{
    this.salirYGuardarCambios.emit();
    this.modalConfirmarCambio.hide();
  }

  // SALIR SIN GUARDAR CAMBIOS
  salirSinGuardar(){
    this.salirSinGuardarCambios.emit();
    this.modalConfirmarCambio.hide();
  }

}
