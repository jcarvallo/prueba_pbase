import { IMyDpOptions } from 'mydatepicker';
import { IMyDrpOptions } from 'mydaterangepicker';

export class OptionsDatePicker{

  public static myDatePickerOptions: IMyDpOptions =
  {
    dateFormat: 'dd-mm-yyyy',
		editableDateField: false,
		selectionTxtFontSize: '1.3rem',
		dayLabels: { su: "Do", mo: "Lu", tu: "Ma", we: "Mi", th: "Ju", fr: "Vi", sa: "Sa" },
		monthLabels: { 1: "Ene", 2: "Feb", 3: "Mar", 4: "Abr", 5: "May", 6: "Jun", 7: "Jul", 8: "Ago", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dic" },
		todayBtnTxt: "Hoy",
		firstDayOfWeek: "mo",
		showInputField: true,
		openSelectorOnInputClick: true
  };

  public static myDateRangePickerOptions: IMyDrpOptions = {
    dateFormat: 'dd-mm-yyyy',
    selectionTxtFontSize: '1.3rem',
    dayLabels: { su: "Do", mo: "Lu", tu: "Ma", we: "Mi", th: "Ju", fr: "Vi", sa: "Sa" },
    monthLabels: { 1: "Ene", 2: "Feb", 3: "Mar", 4: "Abr", 5: "May", 6: "Jun", 7: "Jul", 8: "Ago", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dic" },
    firstDayOfWeek: "mo",
    openSelectorOnInputClick: true,
    editableDateRangeField: false,
    height: '29px',
    width: '100%',
    selectBeginDateTxt: 'Fecha de inicio',
    selectEndDateTxt: 'Fecha final',
    showClearDateRangeBtn: false,
    showClearBtn: false
}

}
