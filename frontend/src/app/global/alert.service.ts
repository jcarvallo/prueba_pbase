import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';
import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { environment } from '../../environments/environment';

@Injectable()
export class AlertServer {
  apiUrl = environment.apiUrl;
  currentUser: any;

  constructor(
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig,
    private router: Router,
  ) {
    // OPCIONES PREDETERMINADAS TASTY
    this.toastyConfig.theme = 'bootstrap';
    this.toastyConfig.timeout = 5000;
    this.toastyConfig.showClose = true;

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  // MENSAJES DE NOTIFICACION
  message(message: string, type: string) {
    if (type == "success") {
      this.toastyService.success(message);
    }
    if (type == "warning") {
      this.toastyService.warning(message);
    }
    if (type == "info") {
      this.toastyService.info(message);
    }
    if (type == "error") {
      this.toastyService.error(message);
    }
    if (type == "default") {
      this.toastyService.default(message);
    }

    return true;
  }

  // MENSAJES DE ERROR
  messageError(error: any): void {
    let messageError = JSON.parse(error._body).error;
    this.toastyService.error(messageError);

    if (error.status== 401) this.router.navigate(['/login']);
  }

}
