import createNumberMask from 'text-mask-addons/dist/createNumberMask';

export class TextMask{

  public static timeMask = [/[0-9]/, ':', /[0-5]/, /[0-9]/];

  public static onlyNumberMask = createNumberMask({
    prefix: '',
    suffix: '',
    decimalSymbol: '',
    thousandsSeparatorSymbol: '',
  });

}
