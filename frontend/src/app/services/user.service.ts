import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { User } from '../models/index';
import { environment } from '../../environments/environment';

@Injectable()
export class UserService {
  headers: Headers;
  apiUrl = environment.apiUrl;

  constructor(private http: Http) {
    this.jwt();
  }

  getAll() {
    return this.http.get(this.apiUrl + 'user', this.jwt()).map((response: Response) => response.json());
  }

  create(create: User) {
    return this.http.post(this.apiUrl + 'users', create, this.jwt()).map((response: Response) => response.json());
  }

  update(update: User, id: number) {
    return this.http.put(this.apiUrl + 'users/' + id, update, this.jwt()).map((response: Response) => response.json());
  }

  delete(id: number) {
    return this.http.delete(this.apiUrl + 'users/' + id, this.jwt()).map((response: Response) => response.json());
  }

  getListado() {
    return this.http.get(this.apiUrl + 'listado-users', this.jwt()).map((response: Response) => response.json());
  }

  enviarEmail(user: User) {
    return this.http.post(this.apiUrl + 'envio-email-cambio-contrasena', user).map((response: Response) => response.json());
  }

  getFilter(filter: any) {
    let params = new URLSearchParams();
    params.set('nombre', filter.nombre);
    params.set('nombre_ord', filter.nombre_ord);
    params.set('apellido', filter.apellido);
    params.set('apellido_ord', filter.apellido_ord);
    params.set('email', filter.email);
    params.set('email_ord', filter.email_ord);
    params.set('telefono', filter.telefono);
    params.set('telefono_ord', filter.telefono_ord);
    params.set('rol', filter.rol);
    params.set('rol_ord', filter.rol_ord);
    params.set('habilitado', filter.habilitado);
    params.set('pagina', filter.pagina);
    params.set('action', 'search');
    params.set('format', 'json');
    params.set('callback', 'JSONP_CALLBACK');

    let options = new RequestOptions({ headers: this.headers, search: params });

    return this.http.get(this.apiUrl + 'users', options).map((response: Response) => response.json());
  }

  private jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      this.headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + currentUser.token });
      return new RequestOptions({ headers: this.headers });
    }
  }

}
