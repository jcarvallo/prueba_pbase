import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthService {
  apiUrl = environment.apiUrl;
  constructor(private http: Http) { }

  login(email: string, password: string) {
    return this.http.post(this.apiUrl + 'login', ({ email: email, password: password }))
      .map((response: Response) => {
        let data = response.json();
        if (data.data && data.data.token) {
          localStorage.setItem('currentUser', JSON.stringify(data.data));
        }
        return data.data;
      });
  }

  logout() {
    localStorage.removeItem('currentUser');
  }
}
