import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Rol } from '../models/index';
import { environment } from '../../environments/environment';

@Injectable()
export class RolService {
  headers: Headers;
  apiUrl = environment.apiUrl;

  constructor(private http: Http) {
    this.jwt();
  }

  getAll() {
    return this.http.get(this.apiUrl + 'roles', this.jwt()).map((response: Response) => response.json());
  }

  create(create: Rol) {
    return this.http.post(this.apiUrl + 'roles', create, this.jwt()).map((response: Response) => response.json());
  }

  update(update: Rol, id: number) {
    return this.http.put(this.apiUrl + 'roles/' + id, update, this.jwt()).map((response: Response) => response.json());
  }

  delete(id: number) {
    return this.http.delete(this.apiUrl + 'roles/' + id, this.jwt()).map((response: Response) => response.json());
  }

  getCmbRoles() {
    return this.http.get(this.apiUrl + 'cmb-roles', this.jwt()).map((response: Response) => response.json());
  }

  private jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      this.headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + currentUser.token });
      return new RequestOptions({ headers: this.headers });
    }
  }

}
