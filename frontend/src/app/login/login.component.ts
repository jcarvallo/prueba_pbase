import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, NgForm, FormGroup } from '@angular/forms';
import { AuthService, UserService } from '../services/index';
import { AlertServer } from '../global/index';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  moduleId: module.id,
  selector: 'login',
  templateUrl: './login.html',
  providers: [AlertServer, AuthService, UserService]
})

export class LoginComponent implements OnInit {
  @ViewChild('modalEmail') public modalEmail: ModalDirective;
  formLogin: FormGroup;
  loader: boolean = false;
  isSubmitEmail: boolean = false;
  formEmail: FormGroup;

  constructor(
    private router: Router,
    private auth: AuthService,
    private fb: FormBuilder,
    private notificacion: AlertServer,
    private service: UserService
  ) {
    this.createForm();
  }

  // CREACION DE FORMULARIO DE LOGIN
  createForm() {
    this.formLogin = this.fb.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern(/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/im)
      ])],
      password: ['', Validators.compose([
        Validators.required,
      ])]
    })

    this.formEmail = this.fb.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern(/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/im)
      ])],
    })
  }

  // CARGA AUTOMATICA
  ngOnInit() {
    this.auth.logout();
  }

  // POST INICIO DE SESION
  login() {
    if (this.formLogin.valid) {
      this.loader = true;
      this.auth.login(this.formLogin.get('email').value, this.formLogin.get('password').value)
        .subscribe(
          data => {
            this.router.navigate(['/home']);
            this.loader = false;
          },
          error => {
            this.notificacion.messageError(error);
            this.loader = false;
          }
        );
    } else {
      // VALIDACION ANTES DE POST
      this.formLogin = this.fb.group({
        email: [this.formLogin.get('email').value, Validators.compose([
          Validators.required,
          Validators.pattern(/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/im)
        ])],
        password: [this.formLogin.get('password').value, Validators.compose([
          Validators.required,
        ])]
      })
    }

  }

  // ABRIR MODAL DE CAMBIO DE CONTRASEÑA
  showModal(): void {
    this.modalEmail.show();
  }

  // CERRAR MODAL DE CAMBIO DE CONTRASEÑA
  hideModal(): void {
    this.modalEmail.hide();
    this.formEmail.reset();
    this.isSubmitEmail = false;
  }

  // CAMBIAR CONTRASEÑA
  enviarEmail() {
    this.isSubmitEmail = true;
    if (this.formEmail.valid && this.isSubmitEmail) {

      let model: any = {
        email: this.formEmail.get('email').value,
      }
      this.loader = true;
      this.service.enviarEmail(model)
        .subscribe(
        data => {
          this.hideModal();
          this.notificacion.message("Se envió un email con las instrucciones del cambio de contraseña", "success");
          this.loader = false;
          this.isSubmitEmail = false;
          this.formEmail.reset();
        },
        error => {
          this.notificacion.messageError(error);
          this.loader = false;
          this.isSubmitEmail = false;
        }
        );

    } else {
      this.formEmail = this.fb.group({
        email: [this.formEmail.get('email').value, Validators.compose([
          Validators.required,
          Validators.pattern(/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/im)
        ])],
      })
    }

  }

}
