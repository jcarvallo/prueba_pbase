import { Rol } from './index';

export class User{
  id: number;
  nombre: string;
  apellido: string;
  email:string;
  telefono: string;
  habilitado: boolean;
  rol_id: number;
  rol: Rol;
}
