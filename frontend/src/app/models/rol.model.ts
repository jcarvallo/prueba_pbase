import { User } from './index';

export class Rol{
  id: number;
  nombre: string;
  users: User[];
}
