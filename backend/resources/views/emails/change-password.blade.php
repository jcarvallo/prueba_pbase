<!DOCTYPE html>
<html>
<head>
  <title>Instrucciones para el cambio de contraseña</title>
</head>
<body>

    <div style="width:100%;max-width:650px;text-align:center;background-color:#525E64;font-family:'Montserrat',sans-serif;color:#1a1a1a;">

        <div style="padding:50px;text-align:left;">

            <p style="color:#fff">Hola <strong>{{ $fullname }}</strong>, ingresá al siguiente enlace para cambiar tu contraseña: <br>
              <a href="{{ $link = url('password/reset'.'?token='.$token) }}"> {{ $link }} </a>
            </p>

        </div>

    </div>

</body>
</html>
