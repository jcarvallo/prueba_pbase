<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Exception;

class UserService
{

   /**
   * Filtrado de usuario
   * @param  Request $filter
   * @return Array tipo, mensaje, codigo
   */
   static function getByFilter($filter)
   {

      try {

         // ORDENAMIENTOS
         if ($filter->nombre_ord) {
            $columna= 'users.nombre';
            $orden= $filter->nombre_ord;

         }elseif ($filter->apellido_ord) {
            $columna= 'users.apellido';
            $orden= $filter->apellido_ord;

         }elseif ($filter->email_ord) {
            $columna= 'users.email';
            $orden= $filter->email_ord;

         }elseif ($filter->telefono_ord) {
            $columna= 'users.telefono';
            $orden= $filter->telefono_ord;

         }elseif ($filter->rol_ord) {
            $columna= 'roles.nombre';
            $orden= $filter->rol_ord;

         }else{
            $columna= 'users.nombre';
            $orden= 'ASC';
         }

         // LIMITE
         $offset= ((int)$filter->pagina) * ((int)env('LIMIT_PAGE'));

         //FILTRADO
         $users= User::
         join('roles','roles.id','=','users.rol_id')
         ->when($filter->nombre, function ($query) use ($filter) {
            $query
            ->where('users.nombre','LIKE','%'.$filter->nombre.'%');
         })
         ->when($filter->apellido, function ($query) use ($filter) {
            $query
            ->where('users.apellido','LIKE','%'.$filter->apellido.'%');
         })
         ->when($filter->email, function ($query) use ($filter) {
            $query
            ->where('users.email','LIKE','%'.$filter->email.'%');
         })
         ->when($filter->telefono, function ($query) use ($filter) {
            $query
            ->where('users.telefono','LIKE','%'.$filter->telefono.'%');
         })
         ->when($filter->rol, function ($query) use ($filter) {
            $query
            ->where('roles.id','=',$filter->rol);
         })
         ->when($filter->habilitado || !$filter->habilitado, function ($query) use ($filter) {
            if ($filter->habilitado) {
               $query
               ->where('users.habilitado','=',$filter->habilitado);
            }
            if (!$filter->habilitado) {
               $query
               ->where('users.habilitado','=',$filter->habilitado);
            }
         })
         ->select('users.*','roles.nombre as nombre_rol')
         ->orderBy($columna,$orden)
         ->offset($offset)
         ->limit(((int)env('LIMIT_PAGE')))
         ->get();

         return array("tipo" => "data", "mensaje" => $users, "codigo" => 200);

      } catch (Exception $e) {
         Log::critical('(SERVICE) No se pudo completar la acción: ' . $e);
         return array("tipo" => "error", "mensaje" => $e->getMessage(), "codigo" => 500);
      }

   }

   /**
   * alta de usuarios
   * @param  Array $data
   * @return Array tipo, mensaje, codigo
   */
   static function createUser($data)
   {

      try {

         $userLogin = JWTAuth::parseToken()->authenticate();
         $verificarEmail= User::where('email',$data['email'])->get();

         if ($verificarEmail->isEmpty()) {

            DB::beginTransaction();
            $user= new User;
            $user->created_by= $userLogin->id;
            $user->password= bcrypt('123456');
            $user->fill($data);
            $user->save();

            DB::commit();
            return array("tipo" => "data", "mensaje" => "Ok", "codigo" => 201);

         }

         return array("tipo" => "error", "mensaje" => "Éste email ya se encuentra registrado, por favor verifique", "codigo" => 400);

      } catch (Exception $e) {
         DB::rollBack();
         Log::critical('(SERVICE) No se pudo completar la acción: ' . $e);
         return array("tipo" => "error", "mensaje" => $e->getMessage(), "codigo" => 500);
      }

   }

   /**
   * edicion de usuarios
   * @param  Array $data
   * @param  Int $id
   * @return Array tipo, mensaje, codigo
   */
   static function updateUser($data, $id)
   {

      try {

         $verificarEmail= User::where('email',$data["email"])->where('id','!=',$id)->get();

         if ($verificarEmail->isEmpty()) {

            DB::beginTransaction();
            $user= User::findOrFail($id);
            $user->fill($data);
            $user->save();

            DB::commit();
            $fila= UserService::getById($id);
            return array("tipo" => "data", "mensaje" => $fila["mensaje"], "codigo" => 200);

         }

         return array("tipo" => "error", "mensaje" => "Éste email ya se encuentra registrado, por favor verifique", "codigo" => 400);

      } catch (Exception $e) {
         DB::rollBack();
         Log::critical('(SERVICE) No se pudo completar la acción: ' . $e);
         return array("tipo" => "error", "mensaje" => $e->getMessage(), "codigo" => 500);
      }

   }

   /**
   * baja de usuarios
   * @param  Int $id
   * @return Array tipo, mensaje, codigo
   */
   static function deleteUser($id)
   {

      try {

         DB::beginTransaction();
         $user= User::findOrFail($id);
         $user->delete();

         DB::commit();
         return array("tipo" => "data", "mensaje" => "Ok", "codigo" => 200);

      } catch (Exception $e) {
         DB::rollBack();
         Log::critical('(SERVICE) No se pudo completar la acción: ' . $e);
         return array("tipo" => "error", "mensaje" => $e->getMessage(), "codigo" => 500);
      }

   }

   /**
   * retornar fila editada
   * @param  Int $id
   * @return Collection
   */
   static function getById($id)
   {

      try {

         $users= User::
         join('roles','roles.id','=','users.rol_id')
         ->select('users.*','roles.nombre as nombre_rol')
         ->where('users.id',$id)
         ->first();

         return array("tipo" => "data", "mensaje" => $users, "codigo" => 200);

      } catch (Exception $e) {
         Log::critical('(SERVICE) No se pudo completar la acción: ' . $e);
         return array("tipo" => "error", "mensaje" => $e->getMessage(), "codigo" => 500);
      }

   }

   /**
   * combo de users
   * @return Array
   */
   static function getAll()
   {

      try {

         $users= User::orderBy('nombre','ASC')->get();
         return array("tipo" => "data", "mensaje" => $users, "codigo" => 200);

      } catch (Exception $e) {
         Log::critical('(SERVICE) No se pudo completar la acción: ' . $e);
         return array("tipo" => "error", "mensaje" => $e->getMessage(), "codigo" => 500);
      }

   }

}
