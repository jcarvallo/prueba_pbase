<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;
use App\Rol;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Exception;

class RolService
{

   /**
   * combo de roles
   * @return Array
   */
   static function getAll()
   {

      try {

         $roles= Rol::orderBy('nombre','ASC')->get();
         return array("tipo" => "data", "mensaje" => $roles, "codigo" => 200);

      } catch (Exception $e) {
         Log::critical('(SERVICE) No se pudo completar la acción: ' . $e);
         return array("tipo" => "error", "mensaje" => $e->getMessage(), "codigo" => 500);
      }

   }


}
