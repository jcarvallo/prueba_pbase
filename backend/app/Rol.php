<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  protected $table = 'roles';
  protected $fillable = [
      'nombre',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'created_by',
      'created_at',
      'updated_at'
  ];

  /////////////////////////
  // RELACIONES DE LA TABLA
  /////////////////////////

  // UN ROL PERTENECE A MUCHOS USUARIOS
  public function users()
  {
    return $this->hasMany('App\User');
  }

}
