<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'users';
    protected $fillable = [
        'nombre',
        'apellido',
        'habilitado',
        'email',
        'telefono',
        'password',
        'rol_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_by',
        'created_at',
        'updated_at',
        'password',
        'remember_token'
    ];

    /////////////////////////
    // RELACIONES DE LA TABLA
    /////////////////////////

    // UN USUARIO SOLO PUEDE TENER UN ROL
    public function rol()
    {
      return $this->belongsTo('App\Rol');
    }

}
