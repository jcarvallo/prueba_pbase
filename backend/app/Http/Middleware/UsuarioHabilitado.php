<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Exception;

class UsuarioHabilitado
{
   /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
   public function handle($request, Closure $next)
   {
      try {

         $user = JWTAuth::parseToken()->authenticate();

         if ($user->habilitado == false) {
            
            return response()->json(["error" => "El usuario está deshabilitado, contáctese con el administrador"], 401);

         }else{

            return $next($request);

         }

      } catch (Exception $e) {
         Log::critical('(CUSTOM MIDDLEWARE) No se pudo completar la acción: ' . $e);
         return array("tipo" => "error", "mensaje" => $e->getMessage(), "codigo" => 500);
      }

   }
}
