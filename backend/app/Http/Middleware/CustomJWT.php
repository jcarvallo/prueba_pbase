<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Exception;

class CustomJWT
{
   /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
   public function handle($request, Closure $next)
   {

      try {

         if (! $user = JWTAuth::parseToken()->authenticate()) {

            return response()->json(["error" => "Usuario no encontrado, intente logueandose nuevamente"], 401);

         }

      } catch (TokenExpiredException $e) {

         Log::critical('(JWT) No se pudo completar la acción: ' . $e);
         return response()->json(["error" => "El token ha expirado, intente logueandose nuevamente"], 401);

      } catch (TokenInvalidException $e) {

         Log::critical('(JWT) No se pudo completar la acción: ' . $e);
         return response()->json(["error" => "El token es inválido, intente logueandose nuevamente"], 401);

      } catch (JWTException $e) {

         Log::critical('(JWT) No se pudo completar la acción: ' . $e);
         return response()->json(["error" => "Error con el token de seguridad, se debe generar otro iniciando sesión nuevamente"], 401);

      }

      // the token is valid and we have found the user via the sub claim
      return $next($request);

   }
}
