<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\RolService;
use Log;
use Exception;

class ComboController extends Controller
{

   /**
   *
   *
   * @return JSON de listado de roles
   */
   public function getRoles()
   {

      try {

         $result= RolService::getAll();
         return response()->json([ $result["tipo"] => $result["mensaje"] ], $result["codigo"]);

      } catch (Exception $e) {
         Log::critical('(CONTROLLER) No se pudo completar la acción: ' . $e);
         return response()->json(["error" => $e->getMessage()], 500);
      }

   }

}
