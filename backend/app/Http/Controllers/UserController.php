<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Services\UserService;
use Hash;
use Mail;
use Log;
use Exception;

class UserController extends Controller
{
   /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
   public function index(Request $request)
   {

      try {

         $result= UserService::getByFilter($request);
         return response()->json([ $result["tipo"] => $result["mensaje"] ], $result["codigo"]);

      } catch (Exception $e) {
         Log::critical('No se pudo completar la acción: ' . $e);
         return response()->json(["error" => $e->getMessage()], 500);
      }

   }

   /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
   public function create()
   {
      //
   }

   /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
   public function store(Request $request)
   {

      try {

         $data= $request->all();
         $result= UserService::createUser($data);

         return response()->json([ $result["tipo"] => $result["mensaje"] ], $result["codigo"]);

      } catch (Exception $e) {
         Log::critical('No se pudo completar la acción: ' . $e);
         return response()->json(["error" => $e->getMessage()], 500);
      }

   }

   /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
   public function show($id)
   {
      //
   }

   /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
   public function edit($id)
   {
      //
   }

   /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
   public function update(Request $request, $id)
   {

      try {

         $data= $request->all();
         $result= UserService::updateUser($data, $id);

         return response()->json([ $result["tipo"] => $result["mensaje"] ], $result["codigo"]);

      } catch (Exception $e) {
         Log::critical('No se pudo completar la acción: ' . $e);
         return response()->json(["error" => $e->getMessage()], 500);
      }

   }

   /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
   public function destroy($id)
   {

      try {

         $result= UserService::deleteUser($id);
         return response()->json([ $result["tipo"] => $result["mensaje"] ], $result["codigo"]);

      } catch (Exception $e) {
         Log::critical('No se pudo completar la acción: ' . $e);
         return response()->json(["error" => $e->getMessage()], 500);
      }

   }

   /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return envio de mail de recuperacion
   */
   public function envioEmailRecuperacion(Request $request)
   {

      try {

         $user= User::where('email','=',$request->email)->first();
         if (count($user) > 0) {

            $token = JWTAuth::fromUser($user);
            $data= array(
               'email' => $user->email,
               'fullname' => $user->nombre . " " . $user->apellido,
               'token' => $token
            );

            Mail::send('emails.change-password', $data, function($msj) use ($data)
            {
               $msj->from(env('MAIL_FROM'), env('MAIL_NAME'));
               $msj->subject('Instrucciones para el cambio de contraseña');
               $msj->to($data['email']);
            });

            return response()->json([ "data" => "Ok" ], 200);

         }else{
            return response()->json([ "error" => "El email ingresado no se encuentra registrado en el sistema" ], 404);
         }

      } catch (Exception $e) {
         Log::critical('No se pudo completar la acción: ' . $e);
         return response()->json(["error" => $e->getMessage()], 500);
      }

   }

}
