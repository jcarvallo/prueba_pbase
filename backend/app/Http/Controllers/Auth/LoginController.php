<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Exception;

class LoginController extends Controller
{
   /*
   |--------------------------------------------------------------------------
   | Login Controller
   |--------------------------------------------------------------------------
   |
   | This controller handles authenticating users for the application and
   | redirecting them to your home screen. The controller uses a trait
   | to conveniently provide its functionality to your applications.
   |
   */

   use AuthenticatesUsers;

   /**
   * Where to redirect users after login.
   *
   * @var string
   */
   protected $redirectTo = '/home';

   /**
   * Create a new controller instance.
   *
   * @return void
   */
   public function __construct()
   {
      $this->middleware('guest')->except('logout');
   }

   public function authenticate(Request $request)
   {

      $credentials = $request->only('email', 'password');
      try {

         if (! $token = JWTAuth::attempt($credentials)) {

            return response()->json(["error" => "Correo electrónico o contraseña incorrecta"], 400);

         }else{
            $user= JWTAuth::toUser($token);
            if ($user->habilitado == true) {

               return response()->json(["data" => compact('user', 'token')], 200);

            }else{

               return response()->json(["error" => "El usuario está deshabilitado, contáctese con el administrador"], 400);

            }
         }

      } catch (JWTException $e) {
         Log::critical('(CONTROLLER) No se pudo completar la acción: ' . $e);
         return response()->json(["error" => $e->getMessage()], 500);
      }

   }

   // INVALIDAR TOKEN EN LOGOUT
   public function logout(Request $request)
   {

      try {

         JWTAuth::invalidate($request->token);
         return response()->json('ok', 200);

      } catch (Exception $e) {
         Log::critical('(CONTROLLER) No se pudo completar la acción: ' . $e);
         return response()->json(["error" => $e->getMessage()], 500);
      }

   }

   // INVALIDAR TOKEN EN LOGOUT WEB DESPUES DE CAMBIAR CONTRASEÑA
   public function logoutWeb(Request $request)
   {

      try {

         JWTAuth::invalidate($request->token);
         return view('password.index');

      } catch (Exception $e) {
         Log::critical('(CONTROLLER) No se pudo completar la acción: ' . $e);
         return response()->json(["error" => $e->getMessage()], 500);
      }

   }

}
