<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\ErrorException;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Log;

class Handler extends ExceptionHandler
{
   /**
   * A list of the exception types that are not reported.
   *
   * @var array
   */
   protected $dontReport = [

   ];

   /**
   * A list of the inputs that are never flashed for validation exceptions.
   *
   * @var array
   */
   protected $dontFlash = [
      'password',
      'password_confirmation',
   ];

   /**
   * Report or log an exception.
   *
   * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
   *
   * @param  \Exception  $exception
   * @return void
   */
   public function report(Exception $exception)
   {
      parent::report($exception);
   }

   /**
   * Render an exception into an HTTP response.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Exception  $exception
   * @return \Illuminate\Http\Response
   */
   public function render($request, Exception $exception)
   {

      // modelo no encontrado
      if ($exception instanceof ModelNotFoundException) {
         Log::critical('(HANDLER) No se pudo completar la acción: ' . $exception);
         return response()->json(["error" => "No existe modelo relacionado a este id"], 404);
      }

      // peticiones fallidas
      if ($exception instanceof NotFoundHttpException) {
         Log::critical('(HANDLER) No se pudo completar la acción: ' . $exception);
         return response()->json(["error" => "No se encontro la url especificada"], 404);
      }

      if ($exception instanceof MethodNotAllowedHttpException) {
         Log::critical('(HANDLER) No se pudo completar la acción: ' . $exception);
         return response()->json(["error" => "El metodo especificado en la peticion no es valido"], 405);
      }

      // problemas con query
      if ($exception instanceof QueryException) {
         Log::critical('(HANDLER) No se pudo completar la acción: ' . $exception);
         return response()->json(["error" => $exception->getMessage()], 500);
      }

      // pdo exception
      if ($exception instanceof PDOException) {
         Log::critical('(HANDLER) No se pudo completar la acción: ' . $exception);
         return response()->json(["error" => $exception->getMessage()], 500);
      }

      // argumento invalid
      if ($exception instanceof InvalidArgumentException) {
         Log::critical('(HANDLER) No se pudo completar la acción: ' . $exception);
         return response()->json(["error" => $exception->getMessage()], 500);
      }

      // error exception
      if ($exception instanceof ErrorException) {
         Log::critical('(HANDLER) No se pudo completar la acción: ' . $exception);
         return response()->json(["error" => $exception->getMessage()], 500);
      }

      // mal metodo llamado
      if ($exception instanceof BadMethodCallException) {
         Log::critical('(HANDLER) No se pudo completar la acción: ' . $exception);
         return response()->json(["error" => $exception->getMessage()], 500);
      }

      // error de sintaxys
      if ($exception instanceof FatalThrowableError) {
         Log::critical('(HANDLER) No se pudo completar la acción: ' . $exception);
         return response()->json(["error" => $exception->getMessage()], 500);
      }

      Log::critical('(HANDLER) No se pudo completar la acción: ' . $exception);
      return response()->json(["error" => $exception->getMessage()], 500);
      // return parent::render($request, $exception);

   }
}
