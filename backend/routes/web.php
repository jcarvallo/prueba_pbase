<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('password/reset', [
	'uses' 	=> 'Auth\ResetPasswordController@getChangePassword',
	'as' 	=> 'password.reset'
]);

Route::post('password/reset', [
	'uses' 	=> 'Auth\ResetPasswordController@postChangePassword',
	'as' 	=> 'password.reset'
]);

Route::get('logout', 'Auth\LoginController@logoutWeb');
