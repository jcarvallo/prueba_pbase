<?php

use Illuminate\Http\Request;

// rutas afuera de proteccion de token
Route::prefix('v1')->middleware(['cors'])->group(function () {
   Route::post('login', 'Auth\LoginController@authenticate');
   Route::post('envio-email-cambio-contrasena', 'UserController@envioEmailRecuperacion');
   Route::get('test', 'TestController@test');
});

// rutas de acceso a traves de tokens
Route::prefix('v1')->middleware(['jwt.auth', 'cors', 'habilitado'])->group(function () {

   // logout u destruccion de token
   Route::get('logout', 'Auth\LoginController@logout');

   // rutas para usuarios
   Route::resource('users', 'UserController');

   // rutas combos
   Route::get('cmb-roles', 'ComboController@getRoles');

});
