<?php

use Illuminate\Database\Seeder;
use App\Rol;
use App\User;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      // rol administrador
      $superadmin= new Rol;
      $superadmin->nombre= "Superadmin";
      $superadmin->save();

      // rol administrador
      $admin= new Rol;
      $admin->nombre= "Administrador";
      $admin->save();

      // rol operador
      $operador= new Rol;
      $operador->nombre= "Operador";
      $operador->save();

      // rol verificador
      $verificador= new Rol;
      $verificador->nombre= "Verificador";
      $verificador->save();

      // rol pagador
      $pagador= new Rol;
      $pagador->nombre= "Pagador";
      $pagador->save();

      // user
      $user= new User;
      $user->nombre= "Administrador";
      $user->apellido= "Tecsolutions";
      $user->habilitado= 1;
      $user->email= "test@mail.com";
      $user->telefono= "123456789";
      $user->password= bcrypt('123456');
      $user->created_by = 1;
      $user->rol_id= $superadmin->id;
      $user->save();

      $faker= Faker::create();
      // creacion de 100 registros de clientes
      for ($i=1; $i < 100; $i++) {

         $user= new User;
         $user->nombre= $faker->name;
         $user->apellido= $faker->lastname;
         $user->habilitado= 1;
         $user->email= $faker->email;
         $user->telefono= "123456789";
         $user->password= bcrypt('123456');
         $user->created_by = 1;
         $user->rol_id= 3;
         $user->save();

      }

    }
}
